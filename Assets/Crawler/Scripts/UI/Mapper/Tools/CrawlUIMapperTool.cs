using UnityEngine;
using UnityEngine.Tilemaps;
public abstract class CrawlUIMapperTool {

	protected readonly CrawlUIMapper mapper;
	protected readonly RectTransform container;
	protected readonly Tilemap tilemap;
	protected readonly CrawlResTiles tiles;

	private bool bMouse;

	public CrawlUIMapperTool(CrawlUIMapper mapper_ui) {
		mapper = mapper_ui;
		container = mapper.refContainer;
		tilemap = mapper.refTilemap;
		tiles = mapper.refTiles;
	}

	protected CrawlMap activeMap { get{ return mapper.activeMap; } }
	protected CrawlMapLayer activeLayer { get{ return mapper.activeLayer; } }
	protected ushort selectedTile { get{ return mapper.selectedTile; } }
	
	protected bool mousePressed { get { return bMouse; } }

	public virtual void process() { }

	public virtual void activate() { }
	public virtual void deactivate() {
		bMouse = false;
	}

	public virtual void onMouseDown() {
		bMouse = mapper.root.leftButton;
	}
	public virtual void onMouseUp() {
		bMouse = mapper.root.leftButton;
	}

	public virtual void onTileChange() { }
}