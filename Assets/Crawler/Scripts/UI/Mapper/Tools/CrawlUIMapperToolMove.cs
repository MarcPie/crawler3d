using UnityEngine;
using UnityEngine.Tilemaps;
public class CrawlUIMapperToolMove : CrawlUIMapperTool {
	private RectTransform unityTileset;
	private Vector2 geomSize = new Vector2();
	private Vector3 geomDragAnchor = new Vector3();

	public CrawlUIMapperToolMove(CrawlUIMapper mapper_ui) : base(mapper_ui) {
		unityTileset = (RectTransform)tilemap.transform;
		geomSize.x = mapper.activeMap.description.width*unityTileset.localScale.x;
		geomSize.y = mapper.activeMap.description.height*unityTileset.localScale.y;
	}

	override public void process() {
		if( !mousePressed ) return;

		Vector3 mouse_pos = mapper.root.getMousePositionIn(container);
		Vector3 dest = new Vector3(mouse_pos.x + geomDragAnchor.x, mouse_pos.y + geomDragAnchor.y, mouse_pos.z);
		Rect bounds = container.rect;
		if( dest.x > bounds.max.x ) dest.x = bounds.max.x;
		else if( dest.x + geomSize.x < bounds.min.x ) dest.x = bounds.min.x - geomSize.x;

		if( dest.y > bounds.max.y ) dest.y = bounds.max.y;
		else if( dest.y + geomSize.y < bounds.min.y ) dest.y = bounds.min.y - geomSize.y;

		unityTileset.localPosition = new Vector3(dest.x, dest.y, 0);
	}

	override public void onMouseDown() {
		base.onMouseDown();
        Vector3 map_mouse = mapper.root.getMousePositionIn(container);
        geomDragAnchor.x = unityTileset.localPosition.x - map_mouse.x;
        geomDragAnchor.y = unityTileset.localPosition.y - map_mouse.y;
	}
}