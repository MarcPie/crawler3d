using UnityEngine;
using UnityEngine.Tilemaps;
public class CrawlUIMapperToolEraser : CrawlUIMapperTool {
	private int nShiftZ;
	public CrawlUIMapperToolEraser(CrawlUIMapper mapper_ui, int z_offset)
		: base(mapper_ui) {
		nShiftZ = z_offset;
	}

	override public void process() {
		if( !mousePressed ) return;

		Vector3 mouse_pos = mapper.root.getMousePositionIn(mapper.refTilemap.transform);
		Vector3Int cell = mapper.refTilemap.LocalToCell(mouse_pos);
		if( cell.x < 0 || cell.x >= activeMap.width ) return;
		if( cell.y < 0 || cell.y >= activeMap.height ) return;

		cell.z = (int)activeMap.getIndex(activeLayer)+nShiftZ;
		tilemap.SetTile(cell, null);
		activeLayer.unset((byte)cell.x, (byte)cell.y);
	}
}