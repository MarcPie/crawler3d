using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class CrawlUIMapperToolPaint : CrawlUIMapperTool {
	public delegate void EventAdd(CrawlMapTile tile);

	private Dictionary<CrawlMapLayerType, ICrawlMapLayerBuilder> dictBuilders = new Dictionary<CrawlMapLayerType, ICrawlMapLayerBuilder>();

	private int nShiftZ;
	public CrawlUIMapperToolPaint(CrawlUIMapper mapper_ui, int z_offset)
		: base(mapper_ui) {
		nShiftZ = z_offset;
	}

	public void add(CrawlMapLayerType type, ICrawlMapLayerBuilder builder) {
		dictBuilders.Add(type, builder);
	}

	override public void process() {
		if( !mousePressed ) return;

		Vector3 mouse_pos = mapper.root.getMousePositionIn(mapper.refTilemap.transform);
		Vector3Int cell = mapper.refTilemap.LocalToCell(mouse_pos);
		if( cell.x < 0 || cell.x >= activeMap.width ) return;
		if( cell.y < 0 || cell.y >= activeMap.height ) return;

		ICrawlCopyable data = null;
		if( dictBuilders.ContainsKey(activeLayer.type) ) {
			data = dictBuilders[activeLayer.type].build( mapper.refTiles.get(activeLayer.type, selectedTile).type );
		}

		cell.z = (int)activeMap.getIndex(activeLayer)+nShiftZ;
		tilemap.SetTile(cell, tiles.get(mapper.activeLayer.type, selectedTile));
		var unit = activeLayer.set((byte)cell.x, (byte)cell.y, selectedTile, data);

		if( eventAdd != null ) {
			eventAdd(unit.tile);
		}
	}

	public event EventAdd eventAdd;
}