﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CrawlUIMapper : CrawlUI {
	public delegate void EventPaint(CrawlMapLayerType type, CrawlMapTile tile);

	public const ushort LAYER_Z = 1;

	public RectTransform refContainer;
	public Tilemap refTilemap;
	public CrawlResTiles refTiles;

	private readonly Dictionary<CrawlUIMapperToolbarToolType, CrawlUIMapperTool> dictTools = new Dictionary<CrawlUIMapperToolbarToolType, CrawlUIMapperTool>();
	private readonly CrawlMap crawlMap = new CrawlMap(CrawlConstants.MAP_SIZE_WIDTH, CrawlConstants.MAP_SIZE_HEIGHT);
	private CrawlUIMapperTool crawlTool;
	private CrawlMapLayer crawlLayer;
	private CrawlUIMapperToolbarToolType enumTool;
	private Nullable<CrawlUIMapperToolbarToolType> enumToolPending;
	private ushort nSelectedTile = 0;

	override public void init(CrawlUIRoot ui_root, CrawlUIContainer ui_parent) {
		base.init(ui_root, ui_parent);

		UnityEngine.Assertions.Assert.IsNotNull(refContainer);
		UnityEngine.Assertions.Assert.IsNotNull(refTilemap);
		UnityEngine.Assertions.Assert.IsNotNull(refTiles);

		dictTools[CrawlUIMapperToolbarToolType.MOVE] = new CrawlUIMapperToolMove(this);

		CrawlUIMapperToolPaint paint = new CrawlUIMapperToolPaint(this, LAYER_Z);
		paint.add(CrawlMapLayerType.ROOMS, new CrawlMapRooms());
		paint.add(CrawlMapLayerType.ACTIONS, new CrawlMapActions());
		paint.eventAdd += onPaint;
		dictTools[CrawlUIMapperToolbarToolType.PAINT] = paint;

		dictTools[CrawlUIMapperToolbarToolType.ERASER] = new CrawlUIMapperToolEraser(this, LAYER_Z);

		crawlTool = dictTools[CrawlUIMapperToolbarToolType.MOVE];
		crawlLayer = crawlMap.layerRooms;

		redraw();

		eventMouseDown += onMouseDown;
		root.eventMouseUp += onMouseUp;
	}

	public CrawlMap activeMap { get { return crawlMap; } }
	public CrawlMapLayer activeLayer { get { return crawlLayer; } }
	public CrawlUIMapperToolbarToolType activeTool { get {return enumTool;} }

	public ushort selectedTile {
		get { return nSelectedTile; }
		set {
			nSelectedTile = value;
			crawlTool?.onTileChange();
		}
	}

	override public void process() {
		base.process();
		crawlTool.process();
	}

	public void switchTool(CrawlUIMapperToolbarToolType tool) {
		if( !dictTools.ContainsKey(tool) ) return;
		if( root.leftButton ) {
			enumToolPending = tool;
		}
		else if( tool != enumTool ) {
			enumTool = tool;
			crawlTool.deactivate();
			crawlTool = dictTools[tool];
			crawlTool.activate();
		}
	}

	public void switchMap(CrawlMap new_map) {
		crawlTool.deactivate();
		crawlMap.clear();
		crawlMap.copyFrom(new_map);
		redraw();
	}

	public void switchLayer(ushort layer_idx) {
		crawlLayer = crawlMap.get(layer_idx);
	}
	public void switchLayer(CrawlMapLayer layer) {
		if( !crawlMap.has(layer) ) throw new ArgumentException("Unknown layer supplied: " + layer + ", is not present in the current map");
		crawlLayer = layer;
	}

	private void redraw() {
		refTilemap.ClearAllTiles();
		refTilemap.SetTilesBlock(crawlMap.description.bounds, refTiles.getSpecial(crawlMap.width, crawlMap.height, CrawResTileSpecial.GRID));

		int z = LAYER_Z;
		foreach( var layer in crawlMap.layers ) {
			CrawlMapSection section = layer.getSection(0, 0, crawlMap.width, crawlMap.height);
			refTilemap.SetTilesBlock(section.toBounds(z), refTiles.get(section));
			z ++;
		}
	}

	public event EventPaint eventPaint;

	private void onPaint(CrawlMapTile tile) {
		if( eventPaint != null ) {
			eventPaint(activeLayer.type, tile);
		}
	}

	private void onMouseDown(CrawlUI target) {
		crawlTool.onMouseDown();
	}
	private void onMouseUp(CrawlUI target) {
		crawlTool.onMouseUp();
		if( enumToolPending.HasValue ) {
			switchTool(enumToolPending.Value);
			enumToolPending = null;
		}
	}

}
