﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlUIMapperToolbar : CrawlUIContainer {
    public delegate void EventToolPick(CrawlUIMapperToolbarToolType tool);
    public delegate void EventTool();

    public RectTransform refSelection;
    public CrawlUI refToolMove;
    public CrawlUI refToolSelect;
    public CrawlUI refToolPaint;
    public CrawlUI refToolEraser;
    public CrawlUI refToolSave;
    public CrawlUI refToolLoad;

    private Dictionary<CrawlUIMapperToolbarToolType, CrawlUI> dictTools = new Dictionary<CrawlUIMapperToolbarToolType, CrawlUI>();
    private CrawlUI[] arrTools;

    private CrawlUIMapperToolbarToolType enumSelected = CrawlUIMapperToolbarToolType.MOVE;

	override public void init(CrawlUIRoot ui_root, CrawlUIContainer ui_parent) {
        base.init(ui_root, ui_parent);

        UnityEngine.Assertions.Assert.IsNotNull(refSelection);
        UnityEngine.Assertions.Assert.IsNotNull(refToolMove);
        UnityEngine.Assertions.Assert.IsNotNull(refToolSelect);
        UnityEngine.Assertions.Assert.IsNotNull(refToolPaint);
        UnityEngine.Assertions.Assert.IsNotNull(refToolEraser);
        UnityEngine.Assertions.Assert.IsNotNull(refToolSave);
        UnityEngine.Assertions.Assert.IsNotNull(refToolLoad);

        mousePassthrough = true;

        dictTools[CrawlUIMapperToolbarToolType.MOVE] = refToolMove;
        dictTools[CrawlUIMapperToolbarToolType.SELECT] = refToolSelect;
        dictTools[CrawlUIMapperToolbarToolType.PAINT] = refToolPaint;
        dictTools[CrawlUIMapperToolbarToolType.ERASER] = refToolEraser;
        
        arrTools = new CrawlUI[]{refToolMove, refToolSelect, refToolPaint, refToolEraser};
        for (int i = arrTools.Length - 1; i >= 0 ; i--) {
            arrTools[i].eventMouseDown += onTool;
        }

        refToolSave.eventMouseDown += onToolSave;
        refToolLoad.eventMouseDown += onToolLoad;
    }

	public CrawlUIMapperToolbarToolType selected {
        get{ return enumSelected; }
    }

    public void select(CrawlUIMapperToolbarToolType tool) {
        enumSelected = tool;
        CrawlUI ui = dictTools[tool];
        refSelection.localPosition = ui.transform.localPosition;
    }

    public event EventToolPick eventToolPick;
    public event EventTool eventToolSave;
    public event EventTool eventToolLoad;

    private void onTool(CrawlUI tool) {
        int idx = Array.IndexOf(arrTools, tool);
        select((CrawlUIMapperToolbarToolType)idx);
        if( eventToolPick != null ) eventToolPick(enumSelected);
    }

    private void onToolSave(CrawlUI target) {
		if( eventToolSave != null ) eventToolSave();
	}

    private void onToolLoad(CrawlUI target) {
		if( eventToolLoad != null ) eventToolLoad();
	}
}

public enum CrawlUIMapperToolbarToolType {
    MOVE = 0,
    SELECT = 1,
    PAINT = 2,
    ERASER = 3
}