﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrawlUIMapperLayerPicker : CrawlUI {
	public delegate void LayerPickEvent(CrawlUIMapperLayerPicker target);

	public Button refButtonRooms;
	public Button refButtonActions;
	public RectTransform refSelectionSprite;

	override public void init(CrawlUIRoot ui_root, CrawlUIContainer ui_parent) {
		base.init(ui_root, ui_parent);

		UnityEngine.Assertions.Assert.IsNotNull(refButtonRooms);
		UnityEngine.Assertions.Assert.IsNotNull(refButtonActions);

		refButtonRooms.onClick.AddListener(onRooms);
		refButtonActions.onClick.AddListener(onActions);
	}

	public event LayerPickEvent eventPickActions;
	public event LayerPickEvent eventPickRooms;

	private void onRooms() {
		refSelectionSprite.localPosition = new Vector3( refButtonRooms.transform.localPosition.x, refButtonRooms.transform.localPosition.y, refSelectionSprite.localPosition.z );
		if( eventPickRooms != null ) eventPickRooms(this);
	}

	private void onActions() {
		refSelectionSprite.localPosition = new Vector3( refButtonActions.transform.localPosition.x, refButtonActions.transform.localPosition.y, refSelectionSprite.localPosition.z );
		if( eventPickActions != null ) eventPickActions(this);
	}
}