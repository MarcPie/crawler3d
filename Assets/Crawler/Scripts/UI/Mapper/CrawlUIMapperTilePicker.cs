﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CrawlUIMapperTilePicker : CrawlUI {
    public delegate void EventTilePick(ushort tileId); 

    public RectTransform refPickerSelection;
    public RectTransform refPickerContainer;
    public Tilemap refPickerTilemap;
    public CrawlResTiles refTiles;

    private Vector2 geomTileSize;
    private Vector2 geomTileShift;

    private bool bSelecting;
    private ushort nSelectingIndex;
    private Vector2 geomSelectionPoint;

    private Dictionary<CrawlMapLayerType, ushort> dictSelections = new Dictionary<CrawlMapLayerType, ushort>();
    private CrawlMapLayerType enumLayer = CrawlMapLayerType.ROOMS;
    private ushort nSize;

	public override void init(CrawlUIRoot ui_root, CrawlUIContainer ui_parent) {
        base.init(ui_root, ui_parent);

        UnityEngine.Assertions.Assert.IsNotNull(refPickerSelection);
        UnityEngine.Assertions.Assert.IsNotNull(refPickerContainer);
        UnityEngine.Assertions.Assert.IsNotNull(refPickerTilemap);
        UnityEngine.Assertions.Assert.IsNotNull(refTiles);

        geomTileSize = refPickerTilemap.cellSize;
        geomTileSize.x *= refPickerTilemap.transform.localScale.x;
        geomTileSize.y *= refPickerTilemap.transform.localScale.y;

        geomTileShift = refPickerSelection.pivot;
        geomTileShift.x *= geomTileSize.x;
        geomTileShift.y *= -geomTileSize.y;

        eventMouseDown += onMouseDown;
        root.eventMouseUp += onMouseUp;

        switchLayer(CrawlMapLayerType.ROOMS);
        select(0);
    }

    public ushort selected {
        get{
            if( !dictSelections.ContainsKey(enumLayer) ) return 0;
            return dictSelections[enumLayer];
        }
    }

    public void select(ushort tile) {
        if( tile >= nSize) tile = (ushort)(nSize-1);
        dictSelections[enumLayer] = tile;
        Vector2 pos = new Vector2((uint)(tile*geomTileSize.x), 0) + geomTileShift;
        refPickerSelection.localPosition = new Vector3(pos.x, pos.y, refPickerSelection.localPosition.z);
    }

    public void switchLayer(CrawlMapLayerType to_type) {
        enumLayer = to_type;
        refPickerTilemap.ClearAllTiles();

        CrawlResTile[] tiles = refTiles.get(to_type);
        nSize = (ushort)tiles.Length;
        Vector3Int[] pos = new Vector3Int[nSize];
        for (int i = nSize - 1; i >= 0 ; i--) {
            pos[i] = new Vector3Int(i, 0, 0);
        }

        refPickerTilemap.SetTiles(pos, tiles);
        select( selected );
    }

    public event EventTilePick eventTilePick;

    private void onMouseDown(CrawlUI target) {
        if( !refPickerContainer.rect.Contains(mouseLocalPosition) ) return;
        
        Vector3 mouse_pos = root.getMousePositionIn(refPickerTilemap.transform);
        Vector3Int cell = refPickerTilemap.LocalToCell(mouse_pos);
        CrawlResTile tile = (CrawlResTile)refPickerTilemap.GetTile(cell);
        if( tile != null ) {
            int idx = refTiles.indexOf(enumLayer, tile);
            if( idx == -1 ) return;
            bSelecting = true;
            nSelectingIndex = (ushort)idx;
            geomSelectionPoint = root.mousePosition;
        }
    }

    private void onMouseUp(CrawlUI target) {
        if( !bSelecting ) return;
        
        bSelecting = false;
        Vector2 diff = root.mousePosition - geomSelectionPoint;
        if( diff.magnitude < CrawlConstants.UI_CLICK_MOVEMENT_MARGIN ) {
            select( nSelectingIndex );
            if( eventTilePick != null ) eventTilePick(selected);

        }
    }

}
