using System.Linq;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class CrawlUIRoot : CrawlUIContainer {
	public delegate void EventKeyDown();
	public delegate void EventKeyUp();

	private Canvas unityCanvas;
	private RectTransform transCanvas;

	private List<CrawlUI> collectionMouseChain;
	private CrawlUIDataMouseChain dataMouseChain;
	private bool[] arrButtons = new bool[3];
	private Vector2 geomMouse = new Vector2();
	private Vector3 geomMouseWorld = new Vector3();

	private HashSet<KeyCode> mapKeys = new HashSet<KeyCode>();
	private KeyCode[] arrKeys;

	private Rect geomScreen;

	public Vector2 mousePosition { get{ return geomMouse; } }
	public Vector3 mouseWorldPositon { get{ return geomMouseWorld; } }
	public Rect screenRect { get{ return geomScreen; } }

	public bool leftButton { get{return arrButtons[0];} }
	public bool rightButton { get{return arrButtons[1];} }
	public bool middleButton { get{return arrButtons[2];} }

	override public CrawlUIContainer parent { get{return null; }}

	void Awake() {
		init(this, this);

		unityCanvas = GetComponent<Canvas>();
		transCanvas = (RectTransform)unityCanvas.transform;
		geomScreen = transCanvas.rect;

		collectionMouseChain = new List<CrawlUI>(8);
		collectionMouseChain.Add(this);
		dataMouseChain = new CrawlUIDataMouseChain(collectionMouseChain);

		arrKeys = (KeyCode[])Enum.GetValues(typeof(KeyCode));
		arrKeys = arrKeys.Except(new KeyCode[] {KeyCode.Mouse0, KeyCode.Mouse1, KeyCode.Mouse2, KeyCode.Mouse3, KeyCode.Mouse4, KeyCode.Mouse5, KeyCode.Mouse6}).ToArray<KeyCode>();
	}

	void Update() {
		bool mouse_down = false;
		bool mouse_up = false;
		for (int i = arrButtons.Length - 1; i >= 0 ; i--) {
			bool state = Input.GetMouseButton(i);
			if( state != arrButtons[i] ) {
				arrButtons[i] = state;
				mouse_down = mouse_down || state;
				mouse_up = mouse_up || !state;
			}
		}

		bool key_down = false;
		bool key_up = false;
		for (int i = arrKeys.Length - 1; i >= 0 ; i--) {
			KeyCode code = arrKeys[i];
			if( Input.GetKey(code) && !mapKeys.Contains(code) ) {
				key_down = true;
				mapKeys.Add(code);
			}
			else if( !Input.GetKey(code) && mapKeys.Contains(code) ) {
				key_up = true;
				mapKeys.Remove(code);
			}
		}

		bool mouse_move = false;
		geomMouseWorld = Camera.main.ScreenToWorldPoint( new Vector3(Input.mousePosition.x, Input.mousePosition.y, unityCanvas.planeDistance) );
		Vector3 mouse_pos = unityCanvas.transform.InverseTransformPoint(geomMouseWorld);
		if( mouse_pos.x != geomMouse.x || mouse_pos.y != geomMouse.y) {
			geomMouse.x = (int)mouse_pos.x;
			geomMouse.y = (int)mouse_pos.y;
			mouse_move = true;
		}

		int len = collectionMouseChain.Count;
		for(int i = 1; i < len; i++) {
			CrawlUI ui = findMouseTarget(collectionMouseChain[i-1] as CrawlUIContainer);
			if( !isValidMouseTarget( collectionMouseChain[i] ) || collectionMouseChain[i]!=ui) {
				
				for(int j = len-1; j > i-1; j--) {
					if(collectionMouseChain[j].mouseEnabled) collectionMouseChain[j].dispatchMouseRollOut();
				}
				collectionMouseChain.RemoveRange(i, len-i);
				
				break;
			}
		}
		
		len = collectionMouseChain.Count;
		CrawlUIContainer container = null;
		if(len>0) container = collectionMouseChain[ len-1 ] as CrawlUIContainer;
		else if(len == 0) {
			container = this;
			collectionMouseChain.Add( this );
		}
			
		while(container != null) {
			CrawlUI ui = findMouseTarget(container);
			if(ui != null) {
				collectionMouseChain.Add(ui);
				ui.dispatchMouseRollOver();
			}
			container = ui as CrawlUIContainer;
		}
		
		if( mouse_up ) {
			onMouseUp();
		}

		if( mouse_down ) {
			onMouseDown();
		}

		if( mouse_move ) {
			onMouseMove();
		}

		if( key_down ) {
			if( eventKeyDown != null ) eventKeyDown();
		}
		if( key_up ) {
			if( eventKeyUp != null ) eventKeyUp();
		}

		process();
	}

	public Vector3 getMousePositionIn(Transform space) {
		return space.InverseTransformPoint(geomMouseWorld);
	}

	private CrawlUI findMouseTarget(CrawlUIContainer container) {
			if( !container.mouseChildren ) return null;
			int length = (int)container.numChildren;
			for (int i = 0; i < length; i++) {
				CrawlUI ui = container.getAt(i);
				if( isValidMouseTarget(ui) ) return ui;
			}
			return null;
		}
	private bool isValidMouseTarget(CrawlUI ui) {
		return ui.mouseEnabled && ui.visible && ui.hitTest(ui.mouseLocalPosition);
	}

	public event EventKeyDown eventKeyDown;
	public event EventKeyUp eventKeyUp;

	private void onMouseDown() {
		int length = collectionMouseChain.Count;
		for (int i = 0; i < length; i++) {
			collectionMouseChain[i].dispatchMouseDown();
		}
	}

	private void onMouseUp() {
		int length = collectionMouseChain.Count;
		for (int i = 0; i < length; i++) {
			collectionMouseChain[i].dispatchMouseUp();
		}
	}

	private void onMouseMove() {
		int length = collectionMouseChain.Count;
		for (int i = 0; i < length; i++) {
			collectionMouseChain[i].dispatchMouseMove();
		}
	}

}