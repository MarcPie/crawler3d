using System;
using System.Collections.Generic;
using UnityEngine;
public class CrawlUIContainer : CrawlUI {
	private IList<CrawlUI> listChildren = new List<CrawlUI>();
	private uint nAmount;

	public bool mouseChildren { get; set; } = true;
	public bool mousePassthrough { get; set; } = false;
	public uint numChildren { get { return (uint)nAmount; } }

	override public void init(CrawlUIRoot ui_root, CrawlUIContainer ui_parent) {
		base.init(ui_root, ui_parent);

		for( int i = transform.childCount- 1; i >= 0; i-- ) {
			var child = transform.GetChild(i);
			//CrawlUI ui = (CrawlUI)child.GetComponent(typeof(CrawlUI));
			CrawlUI ui = child.GetComponent<CrawlUI>();
			if( ui != null ) {
				listChildren.Add(ui);
				ui.init(ui_root, ui_parent);
			}
		}
		nAmount = (uint)listChildren.Count;

		/*
		for( int i = (int)nAmount - 1; i >= 0; i-- ) {
			CrawlUI child = listChildren[i];
			child.init(ui_root, ui_parent);
		}
		*/
	}

	override public void process() {
		for( int i = (int)nAmount - 1; i >= 0; i-- ) {
			listChildren[i].process();
		}
	}

	public virtual CrawlUI getAt(uint idx) {
		return listChildren[(int)idx];
	}
	public virtual CrawlUI getAt(int idx) {
		return listChildren[idx];
	}

	override public bool hitTest(Vector2 pos) {
		if( !mousePassthrough ) return base.hitTest(pos);
		for( int i = 0; i < nAmount; i++ ) {
			CrawlUI child = listChildren[i];
			Vector2 local = child.localMatrix.inverse.MultiplyPoint(pos);
			if( child.hitTest(local) ) return true;
		}
		return false;
	}
}