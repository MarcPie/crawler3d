using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class CrawlUI : MonoBehaviour {
	public delegate void EventMouse(CrawlUI target);

	private CrawlUIRoot uiRoot;
	private CrawlUIContainer uiParent;

	private RectTransform unityTransform;

	public virtual CrawlUIRoot root { get{return uiRoot;} }
	public virtual CrawlUIContainer parent { get{return uiParent;} }

	public virtual RectTransform rectTransform { get{return unityTransform;} }

	public virtual float x {
		get { return unityTransform.localPosition.x; }
		set { setPosition(value, unityTransform.localPosition.y); }
	}

	public virtual float y {
		get { return unityTransform.localPosition.y; }
		set { setPosition(unityTransform.localPosition.x, value); }
	}
	
	public virtual float width {
		get{ return unityTransform.rect.width * unityTransform.localScale.x; }
		set{ setScale(value==0 ? value : (value / unityTransform.rect.width), unityTransform.localScale.y); }
	}

	public virtual float height {
		get{ return unityTransform.rect.height * unityTransform.localScale.y; }
		set{ setScale(unityTransform.localScale.x, value==0 ? value : (value / unityTransform.rect.height) ); }
	}

	public virtual float scaleX {
		get { return unityTransform.localScale.x; }
		set { setScale(value, unityTransform.localScale.y); }
	}

	public virtual float scaleY {
		get { return unityTransform.localScale.y; }
		set { setScale(unityTransform.localScale.x, value); }
	}

	public virtual float rotation {
		get { return unityTransform.localRotation.z; }
		set { unityTransform.Rotate(0, 0, value, Space.Self); }
	}

	public virtual Matrix4x4 localMatrix {
		get { 
			Matrix4x4 matrix = new Matrix4x4();
			matrix.SetTRS(unityTransform.localPosition, unityTransform.localRotation, unityTransform.localScale);
			return matrix;
		}
	}

	public virtual Vector3 mouseLocalPosition { get{return transform.InverseTransformPoint(root.mouseWorldPositon);} }
	public virtual bool mouseEnabled { get; set; } = true;

	public virtual bool visible {
		get { return gameObject.activeSelf; }
		set { gameObject.SetActive(value); }
	}

	public virtual void init(CrawlUIRoot ui_root, CrawlUIContainer ui_parent) {
		uiRoot = ui_root;
		uiParent = ui_parent;
		unityTransform = GetComponent<RectTransform>();
	}

	public virtual void process() { }

	public virtual void setScale(float scale_x, float scale_y) {
		unityTransform.localScale = new Vector3(scale_x, scale_y, unityTransform.localScale.z);
	}
	public virtual void setPosition(float pos_x, float pos_y) {
		unityTransform.localPosition = new Vector3(pos_x, pos_y, unityTransform.localPosition.z);
	}

	public virtual bool hitTest(Vector2 pos) {
		return unityTransform.rect.Contains(pos);
	}

	public event EventMouse eventMouseDown;
	internal virtual void dispatchMouseDown() { if(eventMouseDown!=null) eventMouseDown(this); }

	public event EventMouse eventMouseUp;
	internal virtual void dispatchMouseUp() { if(eventMouseUp!=null) eventMouseUp(this); }

	public event EventMouse eventMouseMove;
	internal virtual void dispatchMouseMove() { if(eventMouseMove!=null) eventMouseMove(this); }

	public event EventMouse eventMouseRollOver;
	internal virtual void dispatchMouseRollOver() { if(eventMouseRollOver!=null) eventMouseRollOver(this); }

	public event EventMouse eventMouseRollOut;
	internal virtual void dispatchMouseRollOut() { if(eventMouseRollOut!=null) eventMouseRollOut(this); }
}