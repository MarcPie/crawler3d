using System;
using System.Collections.Generic;
using System.Collections;
public class CrawlUIDataMouseChain {
	private List<CrawlUI> collectionChain;
	public CrawlUIDataMouseChain(List<CrawlUI> chain) {
		collectionChain = chain;
	}

	public uint length { get{return (uint)collectionChain.Count;} }

	public CrawlUI first { get{ return collectionChain.Count==0 ? null : collectionChain[0];} }
	public CrawlUI last { get{ return collectionChain.Count==0 ? null : collectionChain[collectionChain.Count-1]; } }

	public CrawlUI getNext(CrawlUI parent) {
		int i = collectionChain.IndexOf(parent);
		if(i == -1 || i == collectionChain.Count-1) return null;
		return collectionChain[i+1];
	}
	
	public CrawlUI getPrevious(CrawlUI child) {
		int i = collectionChain.IndexOf(child);
		if(i == -1 || i == 0) return null;
		return collectionChain[i-1];
	}

	public CrawlUI getAt(uint idx) {
		return collectionChain[(int)idx];
	}
	public CrawlUI getAt(int idx) {
		return collectionChain[idx];
	}
	
	override public string ToString() {
		return String.Join("\n", collectionChain);
	}
}