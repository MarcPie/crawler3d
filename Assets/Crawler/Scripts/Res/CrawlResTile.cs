using UnityEngine;
using UnityEngine.Tilemaps;
public class CrawlResTile : Tile {
	private ushort nType;
	public ushort type{ get {return nType;} }

	public static CrawlResTile create(Sprite sprite, ushort type, Color color) {
		CrawlResTile tile = ScriptableObject.CreateInstance<CrawlResTile>();
		tile.sprite = sprite;
		tile.color = color;
		tile.nType = type;
		return tile;
	}
	public static CrawlResTile create(Sprite sprite, ushort type) {
		CrawlResTile tile = ScriptableObject.CreateInstance<CrawlResTile>();
		tile.sprite = sprite;
		tile.color = Color.white;
		tile.nType = type;
		return tile;
	}
}