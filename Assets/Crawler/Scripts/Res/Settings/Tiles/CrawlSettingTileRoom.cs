using UnityEngine;

[CreateAssetMenu(fileName = "CrawlTileRoom", menuName = "Crawl/Tiles/Room", order = 1)]
public class CrawlSettingTileRoom : CrawlSettingTile {
	public CrawlMapRoomType Type;
}