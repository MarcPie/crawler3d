using UnityEngine;

[CreateAssetMenu(fileName = "CrawlTileAction", menuName = "Crawl/Tiles/Action", order = 2)]
public class CrawlSettingTileAction : CrawlSettingTile {
	public CrawlMapActionType Type;
}