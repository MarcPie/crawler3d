using UnityEngine;

[CreateAssetMenu(fileName = "CrawlTile", menuName = "Crawl/Tiles/Tile", order = 0)]
public class CrawlSettingTile : ScriptableObject {
	public Sprite Asset;
}