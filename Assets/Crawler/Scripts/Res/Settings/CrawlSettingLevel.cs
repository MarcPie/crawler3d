﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CrawlLevel", menuName = "Crawl/Level")]
public class CrawlSettingLevel : ScriptableObject {
    public TextAsset Data;
    public CrawlSettingBlocks Blocks;
}
