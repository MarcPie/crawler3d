using UnityEngine;

[CreateAssetMenu(fileName = "CrawlWall", menuName = "Crawl/Blocks/Wall", order = 0)]
public class CrawlSettingBlockWall : CrawlSettingBlock { }