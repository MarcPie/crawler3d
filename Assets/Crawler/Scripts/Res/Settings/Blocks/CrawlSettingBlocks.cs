using System;
using UnityEngine;

[CreateAssetMenu(fileName = "CrawlBlocks", menuName = "Crawl/Blocks lisiting")]
public class CrawlSettingBlocks : ScriptableObject {
	public CrawlSettingBlockWall Wall;
    public CrawlSettingBlockRoom[] Rooms;
}
