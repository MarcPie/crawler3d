using UnityEngine;

[CreateAssetMenu(fileName = "CrawlRoom", menuName = "Crawl/Blocks/Room", order = 1)]
public class CrawlSettingBlockRoom : CrawlSettingBlock {
	public CrawlMapRoomType Type;
}