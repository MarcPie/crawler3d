using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CrawlResTiles : MonoBehaviour {
	public static readonly ushort SPECIAL_TILE_EMPTY = 0;
	public static readonly ushort SPECIAL_TILE_GRID = 1;

	public CrawlSettingTile refEmptyTile;
	public CrawlSettingTile refGridTile;
	public CrawlSettingTileRoom[] refRoomTiles;
	public CrawlSettingTileAction[] refActionTiles;

	private Dictionary<CrawlMapLayerType, CrawlResTile[]> dictTiles = new Dictionary<CrawlMapLayerType, CrawlResTile[]>();
	private CrawlResTile tileEmpty;
	private CrawlResTile tileGrid;

	void OnValidate() {
		if( refEmptyTile == null ) Debug.LogWarning("No 'SpriteEmpty' was set on '" + typeof(CrawlResTiles) + "'");
		if( refGridTile == null ) Debug.LogWarning("No 'SpriteFullGrid' was set on '" + typeof(CrawlResTiles) + "'");
		if( refRoomTiles == null || refRoomTiles.Length == 0 ) Debug.LogWarning("No 'SpritesRoom' were set on '" + typeof(CrawlResTiles) + "'");
		if( refActionTiles == null || refActionTiles.Length == 0 ) Debug.LogWarning("No 'SpritesAction' were set on '" + typeof(CrawlResTiles) + "'");
	}

	void Awake() {
		tileEmpty = CrawlResTile.create(refEmptyTile.Asset, 0);
		tileGrid = CrawlResTile.create(refGridTile.Asset, 0);

		dictTiles[CrawlMapLayerType.INVALID] = new CrawlResTile[] { tileEmpty, tileGrid };

		CrawlResTile[] tiles_rooms = new CrawlResTile[refRoomTiles.GetLength(0)];
		for( int i = tiles_rooms.GetLength(0) - 1; i >= 0; i-- ) {
			var tile = refRoomTiles[i];
			tiles_rooms[i] = CrawlResTile.create(tile.Asset, (ushort)tile.Type);
		}
		dictTiles[CrawlMapLayerType.ROOMS] = tiles_rooms;

		CrawlResTile[] tiles_actions = new CrawlResTile[refActionTiles.GetLength(0)];
		for( int i = tiles_actions.GetLength(0) - 1; i >= 0; i-- ) {
			var action = refActionTiles[i];
			tiles_actions[i] = CrawlResTile.create(action.Asset, (ushort)action.Type);
		}
		dictTiles[CrawlMapLayerType.ACTIONS] = tiles_actions;
	}

	public CrawlResTile get(CrawlMapLayerType layer_type, ushort layer_tile) {
		return dictTiles[layer_type][layer_tile];
	}

	public CrawlResTile[] get(CrawlMapLayerType layer_type) {
		return dictTiles[layer_type];
	}

	public CrawlResTile[] get(CrawlMapSection section) {
		CrawlResTile[] output = new CrawlResTile[section.width*section.height];
		CrawlResTile[] tiles = dictTiles[section.type];
		for( ushort i = 0; i < section.amount; i++ ) {
			Nullable<CrawlMapTile> tile = section.get(i);
			if( tile.HasValue ) {
				output[i] = tiles[tile.Value.id];
			}
		}
		return output;
	}

	public CrawlResTile[] getSpecial(byte width, byte height, CrawResTileSpecial id) {
		CrawlResTile tile = dictTiles[CrawlMapLayerType.INVALID][(ushort)id];
		ushort amount = (ushort)(width*height);
		CrawlResTile[] output = new CrawlResTile[amount];
		for (int i = 0; i < amount; i++) {
			output[i] = tile;
		}
		return output;
	}

	public int indexOf(CrawlMapLayerType layer_type, CrawlResTile tile) {
		return Array.IndexOf(dictTiles[layer_type], tile);
	}
}

public enum CrawResTileSpecial {
	EMPTY = 0,
	GRID = 1
}