using UnityEngine;

public class CrawlGameBlock : MonoBehaviour {
	public CrawlGameBlockPart Ceiling;
	public CrawlGameBlockPart Floor;
	public virtual void assemble(CrawlMapLayer layer, byte x, byte y) {
		uint variant = (uint)(x+y);
		Ceiling.switchObject(variant);
		Ceiling.switchMaterial(variant);
		Floor.switchObject(variant);
		Ceiling.switchMaterial(variant);
		gameObject.SetActive(true);
	}
	public virtual void disassemble() {
		gameObject.SetActive(false);
	}
}