using UnityEngine;

public class CrawlGameBlockPart : MonoBehaviour {
	public GameObject[] Objects;
	public Material[] Materials;
	public Material[] FallbackMaterials;

	private Material[] arrMaterials;
	private PartVariant[] arrVariants;
	private GameObject unityRootObject;
	private Transform unityRootTransform;
	private PartVariant crawlActiveVariant;
	private Quaternion geomRotation;
	private int nRotation = 0;

	void Awake() {
		unityRootTransform = transform;
		unityRootObject = gameObject;
		geomRotation = transform.localRotation;
		arrMaterials = Materials.Length != 0 ? Materials : FallbackMaterials;
		// if BlockPart has no children assume it itself is an variant object
		if( Objects.Length == 0 ) {
			Objects = new GameObject[] { unityRootObject };
		}

		//deactivate all available variants and pick one to be active
		arrVariants = new PartVariant[Objects.Length];
		for( int i = arrVariants.Length - 1; i >= 0; i-- ) {
			var variant = new PartVariant(Objects[i]);
			variant.deactivate();
			arrVariants[i] = variant;
		}
		crawlActiveVariant = arrVariants[0];
		crawlActiveVariant.activate();

		//in case no materials were supplied fetch default applied material
		if( arrMaterials.Length == 0 ) {
			arrMaterials = new Material[] { crawlActiveVariant.material };
		}
		crawlActiveVariant.material = arrMaterials[0];
	}

	public int rotation {
		get { return nRotation; }
		set {
			nRotation = value;
			transform.localRotation = geomRotation;
			transform.Rotate(0, nRotation, 0);
		}
	}

	public void activate() {
		unityRootObject.SetActive(true);
	}
	public void deactivate() {
		unityRootObject.SetActive(false);
	}

	public void switchMaterial(uint material_variant) {
		crawlActiveVariant.material = arrMaterials[material_variant % arrMaterials.Length];
	}

	public void switchObject(uint object_variant) {
		crawlActiveVariant.deactivate();
		crawlActiveVariant = arrVariants[object_variant % arrVariants.Length];
		crawlActiveVariant.activate();
	}


	private class PartVariant {
		public readonly GameObject gameObject;
		public readonly MeshRenderer renderer;
		public PartVariant(GameObject variant_object) {
			gameObject = variant_object;
			renderer = gameObject.GetComponent<MeshRenderer>();
		}
		public Material material {
			get { return renderer.material; }
			set { renderer.material = value; }
		}
		public void activate() => gameObject.SetActive(true);
		public void deactivate() => gameObject.SetActive(false);
	}
}
