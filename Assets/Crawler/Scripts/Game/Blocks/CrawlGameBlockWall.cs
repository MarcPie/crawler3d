using System.Collections.Generic;
using UnityEngine;
public class CrawlGameBlockWall : CrawlGameBlock {
	private const int SHAPE_LEFT = 0x0001;
	private const int SHAPE_RIGHT = 0x0010;
	private const int SHAPE_UP = 0x0100;
	private const int SHAPE_DOWN = 0x1000;

	public CrawlGameBlockPart SingleWall;
	public CrawlGameBlockPart DoubleWall;
	public CrawlGameBlockPart SingleCorner;
	public CrawlGameBlockPart DoubleCorner;
	public CrawlGameBlockPart Column;

	private Dictionary<int, BlockShape> dictShapes = new Dictionary<int, BlockShape>();
	private IList<CrawlGameBlockPart> listParts;

	void Awake() {
		listParts = new List<CrawlGameBlockPart>() { SingleWall, DoubleWall, SingleCorner, DoubleCorner, Column };

		// columns
		dictShapes[SHAPE_UP | SHAPE_RIGHT | SHAPE_LEFT | SHAPE_DOWN] = new BlockShape(Column, 0);
		// double corners
		dictShapes[SHAPE_LEFT | SHAPE_RIGHT | SHAPE_UP] = new BlockShape(DoubleCorner, 180);
		dictShapes[SHAPE_LEFT | SHAPE_RIGHT | SHAPE_DOWN] = new BlockShape(DoubleCorner, 0);
		dictShapes[SHAPE_UP | SHAPE_DOWN | SHAPE_RIGHT] = new BlockShape(DoubleCorner, 270);
		dictShapes[SHAPE_UP | SHAPE_DOWN | SHAPE_LEFT] = new BlockShape(DoubleCorner, 90);
		// double walls
		dictShapes[SHAPE_DOWN | SHAPE_UP] = new BlockShape(DoubleWall, 90);
		dictShapes[SHAPE_LEFT | SHAPE_RIGHT] = new BlockShape(DoubleWall, 0);
		// corners
		dictShapes[SHAPE_RIGHT | SHAPE_UP] = new BlockShape(SingleCorner, 180);
		dictShapes[SHAPE_RIGHT | SHAPE_DOWN] = new BlockShape(SingleCorner, 270);
		dictShapes[SHAPE_LEFT | SHAPE_DOWN] = new BlockShape(SingleCorner, 0);
		dictShapes[SHAPE_LEFT | SHAPE_UP] = new BlockShape(SingleCorner, 90);
		// walls
		dictShapes[SHAPE_DOWN] = new BlockShape(SingleWall, 0);
		dictShapes[SHAPE_LEFT] = new BlockShape(SingleWall, 90);
		dictShapes[SHAPE_UP] = new BlockShape(SingleWall, 180);
		dictShapes[SHAPE_RIGHT] = new BlockShape(SingleWall, 270);
	}

	override public void assemble(CrawlMapLayer layer, byte x, byte y) {
		base.assemble(layer, x, y);
		uint variant = (uint)(x+y);

		foreach( var part in listParts ) {
			part.deactivate();
		}

		int shape = 0x0000;
		if( x > 0 && layer.has((byte)(x - 1), y) ) shape |= SHAPE_LEFT;
		if( x < layer.width && layer.has((byte)(x + 1), y) ) shape |= SHAPE_RIGHT;
		if( y > 0 && layer.has(x, (byte)(y - 1)) ) shape |= SHAPE_DOWN;
		if( y < layer.height && layer.has(x, (byte)(y + 1)) ) shape |= SHAPE_UP;

		if( shape == 0 ) {
			gameObject.SetActive(false);
		}
		else {
			gameObject.SetActive(true);
			dictShapes[shape].activate(variant);
		}
	}

	private class BlockShape {
		public int rotation;
		public CrawlGameBlockPart part;
		public BlockShape(CrawlGameBlockPart shape_part, int shape_rotation) {
			part = shape_part;
			rotation = shape_rotation;
		}
		public void activate(uint variant) {
			part.activate();
			part.switchObject(variant);
			part.rotation = rotation;
			part.switchMaterial( (uint)(rotation/90) );
		}
	}
}