using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
public class CrawlGameVRController : MonoBehaviour {
	public CrawlGamePlayer player;
	public bool enableVR;

	private string sDevice;
	void Awake() {
		player.usePreviewCamera = !enableVR;
		if( enableVR ) {
			foreach( var device in XRSettings.supportedDevices ) {
				Debug.Log("Supported VR device: " + device);
			}
			StartCoroutine(co_enableVR());
		}
	}

	private IEnumerator co_enableVR() {
		foreach( var device in CrawlConstants.DEVICES ) {
			if( XRSettings.enabled || device == sDevice ) break;
			if( String.Compare(XRSettings.loadedDeviceName, device, true) != 0 ) {
				XRSettings.LoadDeviceByName(device);
				Debug.Log("Trying to load VR device: " + device);
				yield return null;
				if( String.Compare(XRSettings.loadedDeviceName, device, true) == 0 ) {
					XRSettings.enabled = true;
					sDevice = device;
					var is_room = XRDevice.SetTrackingSpaceType(TrackingSpaceType.RoomScale);
					Debug.Log("Device loaded! Using roomscale: " + is_room);
					break;
				}
			}
		}
	}
}