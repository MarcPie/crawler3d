﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlGamePlayer : MonoBehaviour {
	public Camera CameraMain;
    public Camera CameraPreview;
    public Light LightPlayer; 

    private Camera cameraActive;
    private Transform unityTransform;
    private Quaternion geomRotation;
    private int nRotation;

    void Awake() {
        unityTransform = transform;
        geomRotation = unityTransform.localRotation;
        nRotation = 0;
    }

    public bool usePreviewCamera {
        get{ return cameraActive == CameraPreview; }
        set{
            cameraActive = value ? CameraPreview : CameraMain;
        }
    }

	void Update() {
        LightPlayer.transform.position = cameraActive.transform.position;
	}

    public void rotateLeft() {
        nRotation = (nRotation-90)%360;
        rotate(nRotation);
    }
    public void rotateRight() {
        nRotation = (nRotation+90)%360;
        rotate(nRotation);
    }

    private void rotate(int angle) {
        unityTransform.localRotation = geomRotation;
        unityTransform.Rotate(0, angle, 0);
    }

}
