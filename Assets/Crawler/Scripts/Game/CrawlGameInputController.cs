﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlGameInputController {
	public delegate void EventMove(CrawlGameInputValue direction_x, CrawlGameInputValue direction_y);
	public delegate void EventRotate(CrawlGameInputValue direction);
	private int nRotation = 0;
	private int nHorizontal = 0;
	private int nVertical = 0;

	public void update() {
        int hori = Math.Sign(Input.GetAxis("Horizontal"));
		if( nHorizontal != hori ) {
			nHorizontal = hori;
			eventMove?.Invoke( (CrawlGameInputValue)Math.Sign(nHorizontal), 0 ); 
		}

		int verti = Math.Sign(Input.GetAxis("Vertical"));
		if( nVertical != verti ) {
			nVertical = verti;
			eventMove?.Invoke(0, (CrawlGameInputValue)Math.Sign(nVertical)); 
		}

		int rot = Math.Sign(Input.GetAxis("Rotation"));
		if( nRotation != rot ) {
			nRotation = rot;
			eventRotate?.Invoke((CrawlGameInputValue)nRotation); 
		}
	}

	public event EventMove eventMove;
	public event EventRotate eventRotate;
}

public enum CrawlGameInputValue {
	NEGATIVE = -1,
	NONE = 0,
	POSITIVE = 1
}