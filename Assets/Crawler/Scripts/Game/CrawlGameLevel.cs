﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlGameLevel : MonoBehaviour {
	private static readonly Vector3Int DIMENSIONS_DEFAUT = new Vector3Int(2,3,2);
	public byte TilesVisibilityRadius = 3;
	public Vector3Int BlockDimensions = DIMENSIONS_DEFAUT;
	//TODO: there probably will have to be an array of "logics" here, something that will define possible interactions with level units (rooms, actions, monsters, items...)
	//		this will have to be a separte list of objects/classes, unrelated to Setting Level (since it can be duplicated) in some kind of dedicated structure

	private GameObject unityObject;
	private Transform unityTransform;

	private CrawlMap crawlMap;
	private CrawlMapLayer<CrawlMapRoom> crawlRooms;
	private CrawlGameLevelBuilder crawlBuilder;

	private CrawlSettingLevel dataLevel;

	private int nRotation = 0;
	private byte nX = 0;
	private byte nY = 0;

	void Awake() {
		unityObject = gameObject;
		unityTransform = transform;
		crawlBuilder = new CrawlGameLevelBuilder();
		if( BlockDimensions.x < DIMENSIONS_DEFAUT.x ) BlockDimensions.x = DIMENSIONS_DEFAUT.x;
		if( BlockDimensions.y < DIMENSIONS_DEFAUT.y ) BlockDimensions.y = DIMENSIONS_DEFAUT.y;
		if( BlockDimensions.z < DIMENSIONS_DEFAUT.z ) BlockDimensions.z = DIMENSIONS_DEFAUT.z;
	}

	public void init(CrawlMap map, CrawlSettingLevel level_data, CrawlMapLayerUnit starting_unit = null) {
		crawlMap = map;
		crawlRooms = crawlMap.layerRooms;
		dataLevel = level_data;
		
		crawlBuilder.init(dataLevel.Blocks);

		starting_unit = starting_unit ?? findStart(crawlMap.layerActions);
		if( starting_unit == null ) {
			throw new ArgumentException("Invalid level supplied, does not contain a starting position");
		}
		else {
			nX = starting_unit.position.x;
			nY = starting_unit.position.y;
			rebuild();
		}
	}

	public void move(Vector2Int shift) => move((sbyte)shift.x, (sbyte)shift.y);
	public void move(sbyte shift_x, sbyte shift_y) {
		if( shift_x == 0 && shift_y == 0 ) return;
		Vector3 dir = Quaternion.AngleAxis(nRotation, Vector3.up) * new Vector3(shift_x, 0, shift_y);

		int x = nX + Mathf.RoundToInt(dir.x);
		if( x < 0 || x >= crawlMap.width ) return;
		int y = nY + Mathf.RoundToInt(dir.z);
		if( y < 0 || y >= crawlMap.height ) return;

		reposition((byte)x, (byte)y);
	}

	public void reposition(Vector2Int position) => reposition((byte)position.x, (byte)position.y);
	public void reposition(byte pos_x, byte pos_y) {
		var tile = crawlRooms.get(pos_x, pos_y);
		if( tile == null ) return;
		print("Moving to "+pos_x+":"+pos_y+" which holds tile: "+tile.data.type);
		nX = pos_x;
		nY = pos_y;
		rebuild();
	}

	public void rotateLeft() {
		nRotation = (nRotation-90)%360;
	}
	public void rotateRight() {
		nRotation = (nRotation+90)%360;
	}

	private CrawlMapLayerUnit findStart(CrawlMapLayer<CrawlMapAction> actions_layer) {
		for( int i = actions_layer.amount - 1; i >= 0; i-- ) {
			var start = actions_layer.get((ushort)i);
			if( start.data.type == CrawlMapActionType.ENTER ) {
				return start;
			}
		}
		return null;
	}

	private void rebuild() {
		for( int i = unityTransform.childCount - 1; i >= 0; i-- ) {
			var child = unityTransform.GetChild(i);
			child.parent = null;
			crawlBuilder.destroy(child.gameObject);
		}

		int distance = TilesVisibilityRadius*TilesVisibilityRadius;
		for( int y = -TilesVisibilityRadius; y <= TilesVisibilityRadius; y++ ) {
			for( int x = -TilesVisibilityRadius; x <= TilesVisibilityRadius; x++ ) {
				if( x*x + y*y > distance ) continue;

				int pos_x = nX + x;
				if( pos_x < 0 || pos_x >= crawlMap.width ) continue;

				int pos_y = nY + y;
				if( pos_y < 0 || pos_y >= crawlMap.height ) continue;

				var block = crawlBuilder.build(crawlRooms, (byte)pos_x, (byte)pos_y);
				if( block == null ) continue;

				block.transform.localPosition = new Vector3(x * BlockDimensions.x, 0, y * BlockDimensions.z);
				block.transform.parent = unityTransform;
			}
		}
	}


}
