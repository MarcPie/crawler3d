using System.Collections.Generic;
using UnityEngine;
public class CrawlGameLevelBuilder {
	private CrawlGameLevelPool poolWall;
	private Dictionary<CrawlMapRoomType,CrawlGameLevelPool> dictRooms = new Dictionary<CrawlMapRoomType, CrawlGameLevelPool>();
	public CrawlGameLevelBuilder() { }

	public void init(CrawlSettingBlocks blocks) {
		if( poolWall != null ) poolWall.clear();
		poolWall = new CrawlGameLevelPool("Wall", blocks.Wall.Block);

		foreach(var pool in dictRooms) {
			pool.Value.clear();
		}
		dictRooms.Clear();

		Debug.Assert(blocks.Rooms.Length != 0, "No room blocks supplied");
		foreach(var room in blocks.Rooms) {
			dictRooms[room.Type] = new CrawlGameLevelPool("Room"+room.Type, room.Block);
		}
	}

	public GameObject build(CrawlMapLayer<CrawlMapRoom> rooms, byte x, byte y) {
		GameObject obj;
		if( rooms.has(x,y) ) {
			var tile = rooms.get(x,y);
			var pool = dictRooms[tile.data.type];
			obj = pool.create();
		}
		else {
			obj = poolWall.create();
		}
		var block = obj.GetComponent<CrawlGameBlock>();
		if( block != null ) block.assemble((CrawlMapLayer)rooms, x, y);
		return obj;
	}

	public void destroy(GameObject obj) {
		obj.SetActive(false);
		var block = obj.GetComponent<CrawlGameBlock>();
		if( block != null ) block.disassemble();

		if( poolWall.matches(obj) ) {
			poolWall.release(obj);
		}
		else foreach(var pool in dictRooms.Values) {
			if(pool.matches(obj)) {
				pool.release(obj);
				return;
			}
		}
	}
}


