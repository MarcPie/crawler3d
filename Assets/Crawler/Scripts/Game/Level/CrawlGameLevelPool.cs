using System.Collections.Generic;
using UnityEngine;

class CrawlGameLevelPool {
	private Stack<GameObject> listPool;
	private GameObject unityBlueprint;
	private string sUID;

	public CrawlGameLevelPool(string uid, GameObject blueprint) {
		unityBlueprint = blueprint;
		listPool = new Stack<GameObject>();
		sUID = uid;
	}

	public bool hasAny() {
		return listPool.Count > 0;
	}

	public GameObject create() {
		GameObject obj;
		if( listPool.Count == 0 ) {
			obj = Object.Instantiate(unityBlueprint);
		}
		else {
			obj = listPool.Pop();
		}
		obj.name = sUID;
		return obj;
	}

	public bool matches(GameObject obj) {
		return obj.name == sUID;
	}

	public void release(GameObject obj) {
		Debug.Assert(obj.name == sUID, "Trying to release unrelated GameObject: "+obj+" (doesn't have name: "+sUID+")");
		Debug.Assert(!obj.activeSelf, "Trying to release an active GameObject: "+obj);
		Debug.Assert(obj.transform.parent == null, "Trying to release a GameObject: "+obj+" still with a parent: "+obj.transform.parent);
		listPool.Push(obj);
	}

	public void clear() {
		foreach(var obj in listPool) {
			Object.Destroy(obj);
		}
	}
}