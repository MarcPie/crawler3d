﻿using System;
using UnityEngine;
public class CrawlGame : CrawlSingleton<CrawlGame> {
	
	public CrawlGamePlayer Player;
	public CrawlGameLevel Level;
	public string[] LevelFiles;

	private CrawlIOFileManager ioManager;
	private CrawlGameInputController gameInput;

	void OnValidate() {
		Debug.Assert(LevelFiles.Length>0, "Level Files array is empty. At least one level is required.");
	}

	void Awake() {
		Debug.Log("Hello CrawlerGame!");
		ioManager = new CrawlIOFileManager();
		gameInput = new CrawlGameInputController();
		gameInput.eventMove += onPlayerMove;
		gameInput.eventRotate += onPlayerRotate;

		RenderSettings.fog = true;
	}

	void Start() {
		//string file = CrawlConstants.FILE_LEVEL_NAME+Cralwer.CURRENT_LEVEL+"."+CrawlConstants.FILE_LEVEL_EXTENSION;
		//CrawlMap map = ioManager.loadMap(CrawlConstants.FILE_LEVEL_NAME);
		CrawlSettingLevel level = Resources.Load<CrawlSettingLevel>(LevelFiles[0]);
		CrawlMap map = ioManager.extractMap(level.Data.bytes);
		print("map size: "+map.width+":"+map.height);
		Level.init(map, level);
	}

	void Update() {
		gameInput.update();
	}

	private void onPlayerMove(CrawlGameInputValue direction_x, CrawlGameInputValue direction_y) {
		Level.move((sbyte)direction_x, (sbyte)direction_y);
	}
	private void onPlayerRotate(CrawlGameInputValue direction) {
		if( direction == CrawlGameInputValue.POSITIVE ) {
			Player.rotateLeft();
			Level.rotateLeft();
		}
		else if( direction == CrawlGameInputValue.NEGATIVE) {
			Player.rotateRight();
			Level.rotateRight();
		}
	}
}
