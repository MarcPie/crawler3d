public struct CrawlMapTile {
	public readonly ushort id;
	public readonly ICrawlCopyable data;
	public CrawlMapTile(ushort tile_id, ICrawlCopyable tile_data) {
		id = tile_id;
		data = tile_data;
	}
}