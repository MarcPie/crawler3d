using System;
using UnityEngine;

public class CrawlMapSection {
	public readonly CrawlMapLayerType type;
	public readonly byte x;
	public readonly byte y;
	public readonly byte width;
	public readonly byte height;
	public readonly ushort amount;

	private readonly Nullable<CrawlMapTile>[] tiles;

	public CrawlMapSection(CrawlMapLayerType section_type) : this(section_type, 0,0,0,0) { }
	public CrawlMapSection(CrawlMapLayerType section_type, byte section_x, byte section_y, byte section_width, byte section_height) {
		type = section_type;
		x = section_x;
		y = section_y;
		width = section_width;
		height = section_height;
		amount = (ushort)(section_width * section_height);
		tiles = new Nullable<CrawlMapTile>[amount];
	}

	public Nullable<CrawlMapTile> get(ushort idx) {
		return tiles[idx];
	}
	public void set(ushort idx, ushort tile_id, ICrawlCopyable tile_data = null) {
		tiles[idx] = new CrawlMapTile(tile_id, tile_data);
	}
	public void set(ushort idx, Nullable<CrawlMapTile> tile = null) {
		tiles[idx] = tile;
	}

	public CrawlGeomPosition getPositionAt(ushort idx) {
		byte x,y;
		CrawlMath.indexToPosition(idx, width, height, out x, out y);
		return new CrawlGeomPosition(x, y);
	}

	public BoundsInt toBounds(int at_z = 0) {
		return new BoundsInt(x, y, at_z, width, height, 1);
	}
}