public interface ICrawlMapLayerBuilder {
	ICrawlCopyable build(ushort tile_id);
}