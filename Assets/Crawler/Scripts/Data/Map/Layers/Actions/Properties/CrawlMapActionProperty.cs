using System;
public class CrawlMapActionProperty {

	public dynamic value;

	private string sName;
	private CrawlMapActionPropertyType eType;

	public CrawlMapActionProperty(string prop_name, CrawlMapActionPropertyType prop_type, dynamic prop_value) {
		sName = prop_name;
		eType = prop_type;
		value = prop_value;
	}
	public string name { get { return sName; } }
	public CrawlMapActionPropertyType type { get { return eType; } }
}