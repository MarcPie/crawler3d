public enum CrawlMapActionPropertyType {
	BYTE,
	SBYTE,
	SHORT,
	USHORT,
	INT,
	UINT,
	FLOAT,
	DOUBLE,
	TEXT
}