using System;
public class CrawlMapActions : ICrawlMapLayerBuilder {
	public ICrawlCopyable build(ushort tile_type) {
		CrawlMapActionType type = (CrawlMapActionType)tile_type;
		switch(type) {
			default: return new CrawlMapAction(type);
		}
	}
}