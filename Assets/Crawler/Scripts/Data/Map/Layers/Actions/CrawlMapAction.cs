using System.Collections.Generic;

public class CrawlMapAction : ICrawlCopyable {
	private Dictionary<string, CrawlMapActionProperty> dictProps;
	private CrawlMapActionType enumType;

	public CrawlMapAction(CrawlMapActionType action_type) {
		enumType = action_type;
		dictProps = new Dictionary<string, CrawlMapActionProperty>();
	}
	public CrawlMapAction(CrawlMapActionType prop_type, Dictionary<string, CrawlMapActionProperty> properties) {
		enumType = prop_type;
		dictProps = properties;
	}

	public CrawlMapActionType type { get{ return enumType; } }

	public CrawlMapActionProperty get(string prop_name) {
		return dictProps[prop_name];
	}

	public ICollection<CrawlMapActionProperty> getAll() {
		return dictProps.Values;
	}

	public void set(CrawlMapActionProperty prop) {
		dictProps[prop.name] = prop;
	}

	public void copyFrom(ICrawlCopyable other_copyable) {
		if( !(other_copyable is CrawlMapAction) ) return;
		CrawlMapAction other = (CrawlMapAction)other_copyable;
		
	}
}