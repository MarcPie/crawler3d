﻿public class CrawlMapLayerUnit<T> : CrawlMapLayerUnit where T : class, ICrawlCopyable {
	public CrawlMapLayerUnit(CrawlMapTile layer_tile, CrawlGeomPosition tile_position)
		: base(layer_tile, tile_position) { }

	public new T data { 
		get { return (T)tile.data; } 
	}
}

public class CrawlMapLayerUnit {
	public readonly CrawlMapTile tile;
	public readonly CrawlGeomPosition position;

	public CrawlMapLayerUnit(CrawlMapTile layer_tile, CrawlGeomPosition tile_position) {
		tile = layer_tile;
		position = tile_position;
	}

	public ICrawlCopyable data { 
		get{ return tile.data; }
	}
}
