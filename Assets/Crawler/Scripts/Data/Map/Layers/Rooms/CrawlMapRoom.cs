public class CrawlMapRoom : ICrawlCopyable {
	private CrawlMapRoomType enumType;
	public CrawlMapRoom(CrawlMapRoomType type) {
		enumType = type;
	}
	public CrawlMapRoomType type { get{ return enumType; } }
	public void copyFrom(ICrawlCopyable other_copyable) {
		enumType = (other_copyable as CrawlMapRoom).enumType;
	}
}

