using System;
public class CrawlMapRooms : ICrawlMapLayerBuilder {
	public ICrawlCopyable build(ushort tile_type) {
		CrawlMapRoomType type = (CrawlMapRoomType)tile_type;
		switch(type) {
			default: return new CrawlMapRoom(type);
		}
	}
}