using System;
using System.Collections.Generic;
public class CrawlMapLayer<T> : CrawlMapLayer where T : class, ICrawlCopyable {
	public CrawlMapLayer(CrawlMapDescription map_description, CrawlMapLayerType layer_type) :
		base(map_description, layer_type) { }

	public new CrawlMapLayerUnit<T> get(byte x, byte y) {
		return (CrawlMapLayerUnit<T>)base.get(x, y);
	}
	public new CrawlMapLayerUnit<T> get(CrawlGeomPosition pos) {
		return (CrawlMapLayerUnit<T>)base.get(pos);
	}
	public new CrawlMapLayerUnit<T> get(ushort idx) {
		return (CrawlMapLayerUnit<T>)base.get(idx);
	}

	public CrawlMapLayerUnit<T> set(byte x, byte y, ushort tile_id, T data = null) {
		return (CrawlMapLayerUnit<T>)base.set(x, y, tile_id, data);
	}
	public new CrawlMapLayerUnit<T> set(byte x, byte y, ushort tile_id, ICrawlCopyable data = null) {
		return (CrawlMapLayerUnit<T>)base.set(x, y, tile_id, data);
	}
	public new CrawlMapLayerUnit<T> set(byte x, byte y, CrawlMapTile tile) {
		return (CrawlMapLayerUnit<T>)base.set(x, y, tile);
	}

	public CrawlMapLayerUnit<T> set(CrawlGeomPosition tile_pos, ushort tile_id, T data = null) {
		return (CrawlMapLayerUnit<T>)base.set(tile_pos, tile_id, data);
	}
	public new CrawlMapLayerUnit<T> set(CrawlGeomPosition tile_pos, ushort tile_id, ICrawlCopyable data = null) {
		return (CrawlMapLayerUnit<T>)base.set(tile_pos, tile_id, data);
	}
	public new CrawlMapLayerUnit<T> set(CrawlGeomPosition tile_pos, CrawlMapTile tile) {
		return (CrawlMapLayerUnit<T>)base.set(tile_pos, tile);
	}

	public CrawlMapLayerUnit<T> set(ushort idx, ushort tile_id, T data = null) {
		return (CrawlMapLayerUnit<T>)base.set(idx, tile_id, data);
	}
	public new CrawlMapLayerUnit<T> set(ushort idx, ushort tile_id, ICrawlCopyable data = null) {
		return (CrawlMapLayerUnit<T>)base.set(idx, tile_id, data);
	}
	public new CrawlMapLayerUnit<T> set(ushort idx, CrawlMapTile tile) {
		return (CrawlMapLayerUnit<T>)base.set(idx, tile);
	}

	public void copyFrom(CrawlMapLayer<T> other_layer) {
		base.copyFrom(other_layer);
	}

	protected override CrawlMapLayerUnit createTile(CrawlGeomPosition tile_pos, CrawlMapTile tile) => new CrawlMapLayerUnit<T>(tile, tile_pos);

	protected override IList<CrawlMapLayerUnit> createTiles() => new List<CrawlMapLayerUnit>();

	protected override CrawlMapSection createSection(byte x, byte y, byte width, byte height) {
		if( x >= this.width ) x = (byte)(this.width - 1);
		if( y >= this.height ) y = (byte)(this.height - 1);
		int x_max = x + width;
		if( x_max > this.width ) width = (byte)(this.width - x); // note, its the width being changed
		int y_max = y + height;
		if( y_max > this.height ) height = (byte)(this.height - y); //note, its the height being changed
		return new CrawlMapSection(type, x, y, width, height);
	}
}

public abstract class CrawlMapLayer {
	public readonly byte width;
	public readonly byte height;

	protected readonly CrawlMapDescription description;
	private CrawlMapLayerType enumType;

	protected IList<CrawlMapLayerUnit> listTiles;
	protected IList<ushort> listPositions;

	public CrawlMapLayer(CrawlMapDescription map_description, CrawlMapLayerType layer_type) {
		description = map_description;
		width = map_description.width;
		height = map_description.height;
		enumType = layer_type;
		listTiles = createTiles();
		listPositions = new List<ushort>(listTiles.Count);
	}

	public CrawlMapLayerType type { get { return enumType; } }

	public ushort amount { get { return (ushort)listTiles.Count; } }

	public bool has(byte x, byte y) {
		return has(new CrawlGeomPosition(x, y));
	}
	public bool has(CrawlGeomPosition position) {
		return listPositions.IndexOf(position.packed) != -1;
	}
	public bool has(CrawlMapLayerUnit tile) {
		return listTiles.IndexOf(tile) != -1;
	}

	public CrawlMapLayerUnit get(byte x, byte y) {
		return get(new CrawlGeomPosition(x, y));
	}
	public CrawlMapLayerUnit get(CrawlGeomPosition position) {
		int idx = listPositions.IndexOf(position.packed);
		if( idx == -1 ) return null;
		return listTiles[idx];
	}
	public CrawlMapLayerUnit get(ushort idx) {
		return listTiles[idx];
	}

	public CrawlMapLayerUnit set(byte tile_x, byte tile_y, ushort tile_id, ICrawlCopyable tile_data = null) {
		return set(new CrawlGeomPosition(tile_x, tile_y), new CrawlMapTile(tile_id, tile_data));
	}
	public CrawlMapLayerUnit set(CrawlGeomPosition tile_position, ushort tile_id, ICrawlCopyable tile_data = null) {
		return set(tile_position, new CrawlMapTile(tile_id, tile_data));
	}
	public CrawlMapLayerUnit set(byte tile_x, byte tile_y, CrawlMapTile tile) {
		return set(new CrawlGeomPosition(tile_x, tile_y), tile);
	}
	public CrawlMapLayerUnit set(CrawlGeomPosition tile_position, CrawlMapTile tile) {
		CrawlMapLayerUnit unit = createTile(tile_position, tile);
		int idx = listPositions.IndexOf(tile_position.packed);
		if( idx != -1 ) {
			listTiles[idx] = unit;
		}
		else {
			listPositions.Add(tile_position.packed);
			listTiles.Add(unit);
		}
		return unit;
	}

	public void unset(ushort idx) {
		listPositions.RemoveAt(idx);
		listTiles.RemoveAt(idx);
	}

	public void unset(byte x, byte y) {
		unset(new CrawlGeomPosition(x,y));
	}

	public void unset(CrawlGeomPosition tile_position) {
		int idx = listPositions.IndexOf(tile_position.packed);
		if( idx != -1 ) {
			listPositions.RemoveAt(idx);
			listTiles.RemoveAt(idx);
		}
	}

	public CrawlMapLayerUnit set(ushort idx, CrawlMapTile tile) {
		CrawlGeomPosition position = CrawlGeomPosition.fromPacked(listPositions[idx]);
		return listTiles[idx] = createTile(position, tile);
	}
	public CrawlMapLayerUnit set(ushort idx, ushort tile_id, ICrawlCopyable data = null) {
		CrawlGeomPosition position = CrawlGeomPosition.fromPacked(listPositions[idx]);
		return listTiles[idx] = createTile(position, new CrawlMapTile(tile_id, data));
	}

	public void clear() {
		listTiles = createTiles();
		listPositions = new List<ushort>();
	}

	public void copyFrom(CrawlMapLayer other_layer) {
		if( other_layer.type != type ) return;
		for( ushort i = 0; i < other_layer.amount; i++ ) {
			CrawlMapLayerUnit unit = other_layer.get(i);
			set(unit.position, unit.tile);
		}
	}

	public IList<CrawlMapLayerUnit> getAll() {
		return listTiles;
	}

	public CrawlMapSection getSection(byte section_x, byte section_y, byte section_width, byte section_height) {
		CrawlMapSection section = createSection(section_x, section_y, section_width, section_height);
		ushort i = 0;
		for( byte y = 0; y < section.height; y++ ) {
			for( byte x = 0; x < section.width; x++ ) {
				CrawlGeomPosition pos = new CrawlGeomPosition((byte)(section_x+x), (byte)(section_y+y));
				int idx = listPositions.IndexOf(pos.packed);
				if( idx != -1 ) {
					CrawlMapLayerUnit tile = listTiles[idx];
					section.set((ushort)i, tile.tile);
				}
				i++;
			}
		}
		return section;
	}

	public void setSection(byte x, byte y, CrawlMapSection section) {
		if( x >= width || y >= height ) return;
		ushort idx = 0;
		for( byte j = y; j < y + section.height; j++ ) {
			for( byte i = x; i < x + section.width; i++ ) {
				if( i < width && j < height ) {
					Nullable<CrawlMapTile> tile = section.get(idx);
					if( tile.HasValue ) {
						set(i, j, tile.Value);
					}
				}
				idx++;
			}
		}
	}

	protected abstract CrawlMapLayerUnit createTile(CrawlGeomPosition tile_positon, CrawlMapTile tile);
	protected abstract IList<CrawlMapLayerUnit> createTiles();
	protected abstract CrawlMapSection createSection(byte x, byte y, byte width, byte height);
}