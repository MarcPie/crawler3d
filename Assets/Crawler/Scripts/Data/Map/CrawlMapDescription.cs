using UnityEngine;
public class CrawlMapDescription {
	private byte nWidth;
    private byte nHeight;
    private BoundsInt geomBounds;
	public CrawlMapDescription(byte map_width, byte map_height) {
		Debug.Assert(map_width<=CrawlConstants.MAP_SIZE_WIDTH && map_height<=CrawlConstants.MAP_SIZE_HEIGHT); 
		nWidth = map_width;
		nHeight = map_height;
		geomBounds = new BoundsInt(0, 0, 0, (int)nWidth, (int)nHeight, 1);
	}

	public byte width { get{ return nWidth; } }
    public byte height { get{ return nHeight; } }
    public BoundsInt bounds { get{ return geomBounds; } }
}