using System;
using System.Collections.Generic;

public class CrawlMap {
	public readonly byte width;
	public readonly byte height;

	protected CrawlMapDescription mapDescription;
	protected CrawlMapLayer<CrawlMapRoom> mapRooms;
	protected CrawlMapLayer<CrawlMapAction> mapActions;

	private IList<CrawlMapLayer> listLayers = new List<CrawlMapLayer>();

	public CrawlMap(byte map_width, byte map_height) {
		width = map_width;
		height = map_height;
		mapDescription = new CrawlMapDescription(map_width, map_height);
		mapRooms = new CrawlMapLayer<CrawlMapRoom>(mapDescription, CrawlMapLayerType.ROOMS);
		mapActions = new CrawlMapLayer<CrawlMapAction>(mapDescription, CrawlMapLayerType.ACTIONS);

		listLayers.Add(mapRooms);
		listLayers.Add(mapActions);
	}

	public CrawlMap(CrawlMapDescription map_desc, CrawlMapLayer<CrawlMapRoom> layer_rooms, CrawlMapLayer<CrawlMapAction> layer_actions) {
		width = map_desc.width;
		height = map_desc.height;
		mapDescription = map_desc;
		mapRooms = layer_rooms;
		mapActions = layer_actions;

		listLayers.Add(mapRooms);
		listLayers.Add(mapActions);
	}

	public CrawlMapDescription description { get { return mapDescription; } }
	public IList<CrawlMapLayer> layers { get { return listLayers; } }
	public CrawlMapLayer<CrawlMapRoom> layerRooms { get { return mapRooms; } }
	public CrawlMapLayer<CrawlMapAction> layerActions { get { return mapActions; } }

	public bool has(CrawlMapLayer layer) {
		return listLayers.IndexOf(layer) != -1;
	}

	public short getIndex(CrawlMapLayer layer) {
		return (short)listLayers.IndexOf(layer);
	}

	public CrawlMapLayer get(ushort layer_idx) {
		return listLayers[layer_idx];
	}

	public void clear() {
		foreach( var layer in listLayers ) {
			layer.clear();
		}
	}

	public void copyFrom(CrawlMap target_map) {
		mapDescription = new CrawlMapDescription(target_map.description.width, target_map.description.height);
		mapRooms.copyFrom(target_map.layerRooms);
		mapActions.copyFrom(target_map.layerActions);
	}
}