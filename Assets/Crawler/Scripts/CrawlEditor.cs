﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.XR;

[RequireComponent(typeof(CrawlResTiles))]
public class CrawlEditor : CrawlSingleton<CrawlEditor> {
	public CrawlUIRoot refRoot;
	public CrawlUIMapper refMapper;
	public CrawlUIMapperTilePicker refMapperTiles;
	public CrawlUIMapperToolbar refMapperToolbar;
	public CrawlUIMapperLayerPicker refMapperLayer;

	private CrawlResTiles crawlTiles;

	private CrawlIOFileManager crawlFile;
	private CrawlUIMapperToolbarToolType enumToolMemory;

	private bool bSpace = false;

	void Awake() {
		UnityEngine.Assertions.Assert.IsNotNull(refRoot, "Editor is missing required reference to: '"+typeof(CrawlUIRoot)+"'");
		UnityEngine.Assertions.Assert.IsNotNull(refMapper, "Editor is missing required reference to: '"+typeof(CrawlUIMapper)+"'");
		UnityEngine.Assertions.Assert.IsNotNull(refMapperTiles, "Editor is missing required reference to: '"+typeof(CrawlUIMapperTilePicker)+"'");
		UnityEngine.Assertions.Assert.IsNotNull(refMapperToolbar, "Editor is missing required reference to: '"+typeof(CrawlUIMapperToolbar)+"'");
		UnityEngine.Assertions.Assert.IsNotNull(refMapperLayer, "Editor is missing required reference to: '"+typeof(CrawlUIMapperLayerPicker)+"'");
		
		crawlTiles = GetComponent<CrawlResTiles>();
		crawlFile = new CrawlIOFileManager();
	}

	void Start() {
		refMapper.selectedTile = refMapperTiles.selected;
		refMapperTiles.switchLayer( refMapper.activeLayer.type );

		refMapper.eventPaint += onMapPaint;

		refMapperToolbar.eventToolPick += onToolPick;
		refMapperToolbar.eventToolSave += onToolSave;
		refMapperToolbar.eventToolLoad += onToolLoad;
		refMapperTiles.eventTilePick += onTilePick;

		refMapperLayer.eventPickRooms += onLayerRooms;
		refMapperLayer.eventPickActions += onLayerActions;

		refRoot.eventKeyUp += onShortcut;
		refRoot.eventKeyDown += onShortcut;

		XRSettings.enabled = false;
	}

	private void onTilePick(ushort tile) => refMapper.selectedTile = tile;
	private void onToolPick(CrawlUIMapperToolbarToolType tool) => refMapper.switchTool(tool);

	private void onShortcut() {
		if( Input.GetKey(KeyCode.Space) && !bSpace ) {
			bSpace = true;
			enumToolMemory = refMapper.activeTool;
			refMapper.switchTool(CrawlUIMapperToolbarToolType.MOVE);
		}
		else if( bSpace ) {
			bSpace = false;
			refMapper.switchTool(enumToolMemory);
		}
	}

	private void onToolLoad() {
		CrawlMap map = crawlFile.loadMap();
		if( map != null ) {
			refMapper.switchMap(map);
		}
	}

	private void onToolSave() {
		crawlFile.saveMap( refMapper.activeMap );
	}

	private void onLayerRooms(CrawlUIMapperLayerPicker target) {
		if( refMapper.activeMap.layerRooms == refMapper.activeLayer ) return;
		refMapper.switchLayer( refMapper.activeMap.layerRooms );
		refMapperTiles.switchLayer( CrawlMapLayerType.ROOMS );
		refMapper.selectedTile = refMapperTiles.selected;
	}
	private void onLayerActions(CrawlUIMapperLayerPicker target) {
		if( refMapper.activeMap.layerActions == refMapper.activeLayer ) return;
		refMapper.switchLayer( refMapper.activeMap.layerActions );
		refMapperTiles.switchLayer( CrawlMapLayerType.ACTIONS );
		refMapper.selectedTile = refMapperTiles.selected;
	}

	private void onMapPaint(CrawlMapLayerType type, CrawlMapTile tile) {
		
	}

}
