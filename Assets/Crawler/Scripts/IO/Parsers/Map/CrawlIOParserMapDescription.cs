using System.IO;
public class CrawlIOParserMapDescription {
	public byte[] serialize(CrawlMapDescription desc) {
		using( MemoryStream memory = new MemoryStream() ) {
			using( BinaryWriter writer = new BinaryWriter(memory) ) {
				writer.Write(desc.width);
				writer.Write(desc.height);
			}
			return memory.ToArray();
		}
	}
	public CrawlMapDescription deserialize(byte[] data) {
		using( MemoryStream memory = new MemoryStream(data) ) {
			using( BinaryReader reader = new BinaryReader(memory) ) {
				return new CrawlMapDescription(reader.ReadByte(), reader.ReadByte());
			}
		}
	}
}