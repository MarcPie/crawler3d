using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlIOParserMapLayer : CrawlIOParser {
	private ICrawlIOParserMapLayerData parserData;
	public CrawlIOParserMapLayer(ICrawlIOParserMapLayerData parser_data) {
		parserData = parser_data;
	}

	public byte[] serialize(CrawlMapLayer layer) {
		using( MemoryStream memory = new MemoryStream() ) {
			using( BinaryWriter writer = new BinaryWriter(memory) ) {
				writer.Write(layer.amount);
				for( ushort i = 0; i < layer.amount; i++ ) {
					CrawlMapLayerUnit tile = layer.get(i);
					writer.Write(tile.position.x);
					writer.Write(tile.position.y);
					writer.Write(tile.tile.id);
					writeArray(writer, parserData.serialize(tile.data));
				}
			}
			return memory.ToArray();
		}
	}
	public CrawlMapLayer deserialize(CrawlMapDescription description, byte[] data) {
		using( MemoryStream memory = new MemoryStream(data) ) {
			using( BinaryReader reader = new BinaryReader(memory) ) {
				CrawlMapLayer layer = parserData.createLayer(description);
				ushort amount = reader.ReadUInt16();
				for( ushort i = 0; i < amount; i++ ) {
					layer.set(
						reader.ReadByte(),
						reader.ReadByte(),
						reader.ReadUInt16(),
						parserData.deserialize(readArray(reader))
					);
				}
				return layer;
			}
		}
	}
}
