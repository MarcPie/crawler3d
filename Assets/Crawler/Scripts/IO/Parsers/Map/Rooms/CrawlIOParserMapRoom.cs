using System.IO;
public class CrawlIOParserMapRoom : ICrawlIOParserMapLayerData {
	public CrawlMapLayer createLayer(CrawlMapDescription description) {
		return new CrawlMapLayer<CrawlMapRoom>(description, CrawlMapLayerType.ROOMS);
	}
	public byte[] serialize(ICrawlCopyable data) {
		CrawlMapRoom room = (CrawlMapRoom)data;
		if( room == null ) return new byte[0];

		using( MemoryStream memory = new MemoryStream() ) {
			using( BinaryWriter writer = new BinaryWriter(memory) ) {
				writer.Write((byte)room.type);
			}
			return memory.ToArray();
		}
	}
	public ICrawlCopyable deserialize(byte[] data) {
		if( data.Length == 0 ) return null;
		
		using( MemoryStream memory = new MemoryStream(data) ) {
			using( BinaryReader reader = new BinaryReader(memory) ) {
				CrawlMapRoomType type = (CrawlMapRoomType)reader.ReadByte();
				return new CrawlMapRoom(type);
			}
		}
	}


}