public interface ICrawlIOParserMapLayerData {
	byte[] serialize(ICrawlCopyable data);
	ICrawlCopyable deserialize(byte[] data);
	CrawlMapLayer createLayer(CrawlMapDescription description);
}