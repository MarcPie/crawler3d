using System.Collections.Generic;
using System;
using System.IO;
public class CrawlIOParserMap : CrawlIOParser {
	private readonly CrawlIOParserMapDescription parserDescription = new CrawlIOParserMapDescription();
	private readonly CrawlIOParserMapLayer parserRooms = new CrawlIOParserMapLayer(new CrawlIOParserMapRoom());
	private readonly CrawlIOParserMapLayer parserActions = new CrawlIOParserMapLayer(new CrawlIOParserMapAction());

	public byte[] serialize(CrawlMap map) {
		using ( MemoryStream memory = new MemoryStream() ) {
			using ( BinaryWriter writer = new BinaryWriter(memory) ) {
				writeArray( writer, parserDescription.serialize(map.description) );

				IList<CrawlMapLayer> layers = map.layers;
				ushort len = (ushort)layers.Count;
				for (int i = 0; i < len; i++) {
					CrawlMapLayer layer = layers[i];
					writer.Write((byte)layer.type);
					switch(layer.type) {
						case CrawlMapLayerType.ROOMS:
							writeArray( writer, parserRooms.serialize(layer) );
							break;
						case CrawlMapLayerType.ACTIONS:
							writeArray( writer, parserActions.serialize(layer) );
							break;
					}
					
				}
			}
			return memory.ToArray();
		}
	}

	public CrawlMap deserialize(byte[] map_data) {
		using ( MemoryStream memory = new MemoryStream(map_data) ) {
			using ( BinaryReader reader = new BinaryReader(memory) ) {
				CrawlMapDescription description = parserDescription.deserialize(readArray(reader));
				CrawlMapLayer<CrawlMapRoom> rooms = null;
				CrawlMapLayer<CrawlMapAction> actions = null;
				while(memory.Position < memory.Length) switch( (CrawlMapLayerType)reader.ReadByte() ) {
					case CrawlMapLayerType.ROOMS:
						rooms = (CrawlMapLayer<CrawlMapRoom>)parserRooms.deserialize(description, readArray(reader));
						break;
					case CrawlMapLayerType.ACTIONS:
						actions = (CrawlMapLayer<CrawlMapAction>)parserActions.deserialize(description, readArray(reader));
						break;
				}
				 
				return new CrawlMap(description, rooms, actions );
			}
		}
	}
}