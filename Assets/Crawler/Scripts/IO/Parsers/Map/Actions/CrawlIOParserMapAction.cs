using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CrawlIOParserMapAction : ICrawlIOParserMapLayerData {
	public byte[] serialize(ICrawlCopyable data) {
		CrawlMapAction action = (CrawlMapAction)data;
		if ( action == null ) return new byte[0];

		using( MemoryStream memory = new MemoryStream() ) {
			using( BinaryWriter writer = new BinaryWriter(memory) ) {
				writer.Write( (byte)action.type );
				ICollection<CrawlMapActionProperty> props = action.getAll();
				writer.Write( (ushort) props.Count);

				foreach( var prop in props ) {
					writer.Write( prop.name );
					writer.Write( (byte)prop.type );

					ICrawlIOParserMapActionProperty parser = CrawlIOParserMapActionProperties.PARSERS[prop.type];
					parser.serialize(writer, prop.value);
				}
			}
			return memory.ToArray();
		}
	}

	public ICrawlCopyable deserialize(byte[] data) {
		if( data.Length == 0 ) return null;
		
		using( MemoryStream memory = new MemoryStream(data) ) {
			using( BinaryReader reader = new BinaryReader(memory) ) {
				CrawlMapActionType type = (CrawlMapActionType)reader.ReadByte();
				CrawlMapAction action = new CrawlMapAction(type);
				ushort amount = reader.ReadUInt16();
				for (int i = 0; i < amount; i++) {
					string pname = reader.ReadString();
					CrawlMapActionPropertyType ptype = (CrawlMapActionPropertyType)reader.ReadByte();
					ICrawlIOParserMapActionProperty parser = CrawlIOParserMapActionProperties.PARSERS[ptype];
					CrawlMapActionProperty prop = new CrawlMapActionProperty(pname, ptype, parser.deserialize(reader));
					action.set(prop);
				}
				return action;
			}
		}
	}

	public CrawlMapLayer createLayer(CrawlMapDescription description) {
		return new CrawlMapLayer<CrawlMapAction>(description, CrawlMapLayerType.ACTIONS);
	}
}