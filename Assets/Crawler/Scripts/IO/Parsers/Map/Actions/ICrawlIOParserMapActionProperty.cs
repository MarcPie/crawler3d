using System.IO;
public interface ICrawlIOParserMapActionProperty {
	void serialize(BinaryWriter writer, dynamic value);
	dynamic deserialize(BinaryReader reader);
}