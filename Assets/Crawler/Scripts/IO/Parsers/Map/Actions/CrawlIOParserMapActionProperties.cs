using System.Collections.Generic;
using System.IO;
//TODO: moze w ogole wywalic ta klase i jednak zrobic switcha? 
//		jest skonczona ilosc elementow ktore streamy obsluguja, wiec nie bedzie to szczegolnie duzy switch
public static class CrawlIOParserMapActionProperties {
	public static readonly Dictionary<CrawlMapActionPropertyType, ICrawlIOParserMapActionProperty> PARSERS = new Dictionary<CrawlMapActionPropertyType, ICrawlIOParserMapActionProperty> {
		{ CrawlMapActionPropertyType.BYTE, new CrawlPropertyByte() },
		{ CrawlMapActionPropertyType.SBYTE, new CrawlPropertySByte() },
		{ CrawlMapActionPropertyType.USHORT, new CrawlPropertyUShort() },
		{ CrawlMapActionPropertyType.SHORT, new CrawlPropertyShort() },
		{ CrawlMapActionPropertyType.UINT, new CrawlPropertyUInt() },
		{ CrawlMapActionPropertyType.INT, new CrawlPropertyInt() },
		{ CrawlMapActionPropertyType.FLOAT, new CrawlPropertyFloat() },
		{ CrawlMapActionPropertyType.DOUBLE, new CrawlPropertyDouble() },
		{ CrawlMapActionPropertyType.TEXT, new CrawlPropertyText() }
	};

	private class CrawlPropertyByte : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadByte();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((byte)value);
	}
	private class CrawlPropertySByte : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadSByte();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((sbyte)value);
	}
	private class CrawlPropertyUShort : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadUInt16();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((ushort)value);
	}
	private class CrawlPropertyShort : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadInt16();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((short)value);
	}
	private class CrawlPropertyUInt : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadUInt32();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((uint)value);
	}
	private class CrawlPropertyInt : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadInt32();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((int)value);
	}
	private class CrawlPropertyFloat : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadSingle();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((float)value);
	}
	private class CrawlPropertyDouble : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadDouble();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((double)value);
	}
	private class CrawlPropertyText : ICrawlIOParserMapActionProperty {
		public dynamic deserialize(BinaryReader reader) => reader.ReadString();
		public void serialize(BinaryWriter writer, dynamic value) => writer.Write((string)value);
	}
}


