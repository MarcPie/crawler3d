using System.IO;
public class CrawlIOParser {
	public byte[] readArray(BinaryReader reader) {
		return reader.ReadBytes(reader.ReadInt32());
	}
	public void writeArray(BinaryWriter writer, byte[] array) {
		writer.Write(array.GetLength(0));
		writer.Write(array);
	}
}