using System;
using System.IO;
using UnityEngine;
using SFB;
public class CrawlIOFileManager {

	private CrawlIOParserMap parserMap = new CrawlIOParserMap();

	private string sPathLevels = Application.dataPath + "/"+CrawlConstants.ASSETS_DIRECTORY+"/"+CrawlConstants.FILE_LEVELS_DIRECTORY+"/";
	private ExtensionFilter[] browserExtensions = new ExtensionFilter[] { new ExtensionFilter("Level file ", CrawlConstants.FILE_LEVEL_EXTENSION) };

	public CrawlIOFileManager() { }

	public void saveMap(CrawlMap map) {
		string path = StandaloneFileBrowser.SaveFilePanel("Save level", sPathLevels, "level", browserExtensions);
		if( path == null || path.Length == 0) return;
		File.WriteAllBytes(path, parserMap.serialize(map));
	}
	public CrawlMap loadMap() {
		string[] paths = StandaloneFileBrowser.OpenFilePanel("Open level", sPathLevels, browserExtensions, false);
		if( paths == null || paths.Length == 0 ) return null;
		string path = paths[0];
		if( path == null || path.Length == 0 ) return null;
		return parserMap.deserialize( File.ReadAllBytes(path) );
	}

	public CrawlMap extractMap(byte[] map_data) {
		return parserMap.deserialize(map_data);
	}
}