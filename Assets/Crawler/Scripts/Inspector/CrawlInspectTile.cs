using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CrawlSettingTile))]
public class CrawlInspectTile : Editor {
	SerializedProperty sprite;

	void OnEnable() {
		sprite = serializedObject.FindProperty("Asset");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		EditorGUILayout.ObjectField(sprite, new GUIContent(" Asset:", AssetPreview.GetAssetPreview(sprite.objectReferenceValue)));
		serializedObject.ApplyModifiedProperties();
	}
}