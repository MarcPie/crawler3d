using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CrawlSettingTileRoom))]
public class CrawlInspectTileRoom : Editor {
	SerializedProperty sprite;
	SerializedProperty type;

	void OnEnable() {
		sprite = serializedObject.FindProperty("Asset");
		type = serializedObject.FindProperty("Type");
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		EditorGUILayout.ObjectField(sprite, new GUIContent(" Asset:", AssetPreview.GetAssetPreview(sprite.objectReferenceValue)));
		EditorGUILayout.PropertyField(type, new GUIContent("Type:"));
		serializedObject.ApplyModifiedProperties();
	}
}