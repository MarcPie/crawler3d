public struct CrawlGeomPosition {
	public readonly byte x;
	public readonly byte y;
	public readonly ushort packed;
	public CrawlGeomPosition(byte pos_x, byte pos_y) {
		x = pos_x;
		y = pos_y;
		packed = (ushort)((x << 8) + y);
	}

	public static CrawlGeomPosition fromPacked(ushort packed_position) {
		byte y = (byte)((packed_position & 0xf0) >> 8);
		byte x = (byte)(packed_position & 0x0f);
		return new CrawlGeomPosition(x, y);
	}
}