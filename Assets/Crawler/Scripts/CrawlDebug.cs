using System.Diagnostics;
using UnityEngine;
class CrawlDebug {
	#if DEBUG 
	public static T GetComponent<T>(MonoBehaviour from) where T : Object {
		T comp = from.GetComponent<T>();
		UnityEngine.Assertions.Assert.IsNotNull(comp, "Failed to retrive required Component '"+typeof(T).FullName+"'");
		return comp;
	}
	#else 
	public static T GetComponent<T>(MonoBehaviour from) {
		T comp = from.GetComponent<T>();
		return comp;
	}
	#endif
}