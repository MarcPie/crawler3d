public interface ICrawlCopyable {
	void copyFrom(ICrawlCopyable other_copyable);
}