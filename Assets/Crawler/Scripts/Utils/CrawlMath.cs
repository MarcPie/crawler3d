using UnityEngine;
public static class CrawlMath {
	public static Vector3 clamp(Vector3 point, Vector3 min, Vector3 max) {
		point.x = Mathf.Clamp(point.x, min.x, max.x);
		point.y = Mathf.Clamp(point.y, min.y, max.y);
		point.z = Mathf.Clamp(point.z, min.z, max.z);
		return point;
	}
	public static Vector3Int clamp(Vector3Int point, Vector3Int min, Vector3Int max) {
		point.x = Mathf.Clamp(point.x, min.x, max.x);
		point.y = Mathf.Clamp(point.y, min.y, max.y);
		point.z = Mathf.Clamp(point.z, min.z, max.z);
		return point;
	}
	public static Vector2 clamp(Vector2 point, Vector2 min, Vector2 max) {
		point.x = Mathf.Clamp(point.x, min.x, max.x);
		point.y = Mathf.Clamp(point.y, min.y, max.y);
		return point;
	}
	public static Vector2Int clamp(Vector2Int point, Vector2Int min, Vector2Int max) {
		point.x = Mathf.Clamp(point.x, min.x, max.x);
		point.y = Mathf.Clamp(point.y, min.y, max.y);
		return point;
	}
	public static void indexToPosition(ushort idx, byte width, byte height, out byte pos_x, out byte pos_y) {
		int y = idx / width;
		int x = idx - (y * width);
		pos_y = (byte)y;
		pos_x = (byte)x;
	}
}