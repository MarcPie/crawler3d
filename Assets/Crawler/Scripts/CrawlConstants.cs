public static class CrawlConstants {
	public const byte MAP_SIZE_WIDTH = 127;
	public const byte MAP_SIZE_HEIGHT = 127;

	public const uint UI_CLICK_MOVEMENT_MARGIN = 10;

	public const string ASSETS_DIRECTORY = "Crawler";
	public const string FILE_LEVELS_DIRECTORY = "Levels";
	public const string FILE_LEVEL_EXTENSION = "crawl";
	public const string FILE_LEVEL_NAME = "level";

	public static readonly string[] DEVICES = new string[]{"Oculus", "OpenVR"};
}