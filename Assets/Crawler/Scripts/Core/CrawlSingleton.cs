using UnityEngine;

public class CrawlSingleton<T> : MonoBehaviour where T : MonoBehaviour {
	private static bool SHUTTIN_DOWN = false;
	private static object LOCK = new object();
	private static T INSTANCE;

	public static T Instance {
		get {
			if( SHUTTIN_DOWN ) {
				Debug.LogWarning("["+typeof(CrawlSingleton<T>)+"] Instance '"+typeof(T)+"' already destroyed. Returning null.");
				return null;
			}

			lock( LOCK ) {
				if( INSTANCE == null ) {
					INSTANCE = (T)FindObjectOfType(typeof(T));

					if( INSTANCE == null ) {
						var singletonObject = new GameObject();
						INSTANCE = singletonObject.AddComponent<T>();
						singletonObject.name = typeof(T).ToString() + " ("+typeof(CrawlSingleton<T>)+")";

						DontDestroyOnLoad(singletonObject);
					}
				}

				return INSTANCE;
			}
		}
	}

	private void OnApplicationQuit() {
		SHUTTIN_DOWN = true;
	}

	private void OnDestroy() {
		SHUTTIN_DOWN = true;
	}
}