# Introduction
This is a repository for a VR dungeon crawler game: *Crawler3D*.
The term "game" is used loosely as this is only a snapshot of the initial prototype with working map editor and in-game view of the created map in VR with basic keyboard movement.

# Map Editor
Core feature of Crawler3D is the map editor, allowing anyone to create and then play any map they want.  
![Scheme](Images/level1.gif) ![Scheme](Images/level1-preview.gif)  
![Scheme](Images/level2.gif) ![Scheme](Images/level2-preview.gif)  

# Playing
A map can be created through the *CrawlerMapper* scene, and then played through the *CrawlerGame* scene. Simply change map's file type from "crawl" to "bytes," move it to Resources, and then supply the path to it to CrawlerGame component.
